#ifndef JS_H_INCLUDED
#define JS_H_INCLUDED

void minify_js(char **minification_buffer);
void minify_js_stripComments(char **minification_buffer);
void minify_js_stripWhitespace(char **minification_buffer);

#endif // JS_H_INCLUDED
