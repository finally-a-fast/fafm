#include <stdlib.h>
#include "options.h"

const char *argp_program_version =
  "fafm 1.0.1";

const char *argp_program_bug_address =
  "<foo@bar.org>";


static char doc[] =
  "fafm -- finally a fast minifier";

static char args_doc[] = "INPUTFILE";

static struct argp_option options[] = {
  {"verbose",  'v', 0,      0,  "Produce verbose output" },
  {"quiet",    'q', 0,      0,  "Don't produce any output" },
  {"silent",   's', 0,      OPTION_ALIAS },
  {"type",     't', "TYPE", 0, "js or css; Default: css"},
  {"output",   'o', "FILE", 0, "Output to FILE instead of standard output" },
  { 0 }
};


static error_t parse_opt (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;

  switch (key)
  {
    case 'q': case 's':
      arguments->silent = 1;
      break;
    case 'v':
      arguments->verbose = 1;
      break;
    case 't':
      arguments->type = arg;
      break;
    case 'o':
      arguments->output_file = arg;
      break;


    case ARGP_KEY_ARG:
      if (state->arg_num >= 1)
      {
        argp_usage (state);
      }

      arguments->input_file = arg;

      break;

    case ARGP_KEY_END:
      if (state->arg_num < 1)
      {
        argp_usage (state);
      }
      break;


    default:
      return ARGP_ERR_UNKNOWN;
  }

  return EXIT_SUCCESS;
}

struct argp argp_parser = { options, parse_opt, args_doc, doc };
