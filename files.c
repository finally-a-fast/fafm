#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "files.h"

int load_file(char *file_name, char **input_file_buffer)
{
  unsigned long file_length = 0;
  FILE *input_file = NULL;

  input_file = fopen(file_name, "rb");
  if (!input_file)
  {
    fprintf(stderr, "Could not open file %s!\n\n", file_name);

    return EXIT_FAILURE;
  }
  else
  {
    fseek(input_file, 0, SEEK_END);
    file_length = ftell(input_file);
    fseek(input_file, 0, SEEK_SET);

    *input_file_buffer = (char *)malloc(file_length + 1);
    if(!*input_file_buffer)
    {
      fprintf(stderr, "Error while allocating memory!\n\n");
      fclose(input_file);
      return EXIT_FAILURE;
    }

    if(!fread(*input_file_buffer, file_length, 1, input_file))
    {
      fprintf(stderr, "Error while loading input file!\n\n");
      fclose(input_file);
     return EXIT_FAILURE;
    }

    fclose(input_file);
  }

  return EXIT_SUCCESS;
}


int write_file(char *file_name, char **output_file_buffer)
{
  FILE *output_file = NULL;

  output_file = fopen(file_name, "wb+");
  if (!output_file)
  {
    fprintf(stderr, "Could not open outputfile %s!\n\n", file_name);

    return EXIT_FAILURE;
  }
  else
  {
    if(!fwrite(*output_file_buffer, sizeof(char), strlen(*output_file_buffer), output_file))
    {
      fprintf(stderr, "Error while writing outputfile %s!\n\n", file_name);
      fclose(output_file);

      return EXIT_FAILURE;
    }

    fclose(output_file);
  }

  return EXIT_SUCCESS;
}
