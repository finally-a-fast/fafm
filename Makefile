
# Makefile for fafm
# Might not be up to date. Please compile with Code::Blocks for now to be sure.

VERSION = 1.0.1

FAFMBIN = /usr/local/bin

INSTALL = install
CC = gcc
DATE = `date +"%y.%m.%d-%H%M"`

ARCH = `gcc -dumpmachine || echo bin`
DEBUG = -fPIC -O2 -g -DFAFM_DEBUG
TARGET = fafm
LIBS = -lm -lpcre2-8
CFLAGS = -g -Wall
SOURCES = $(wildcard *.c)
HEADERS = $(wildcard *.h)
OBJECTS = $(patsubst %.c, %.o, $(SOURCES))

.PHONY: default all clean

default: $(TARGET)
all: default

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

.PRECIOUS: $(TARGET) $(OBJECTS)

$(TARGET): $(OBJECTS)
	$(CC) $(OBJECTS) -Wall $(LIBS) -o $@

clean:
	-rm -f *.o
	-rm -f $(TARGET)

install: checkinst
	$(INSTALL) -m 0755 -d $(FAFMBIN)
	$(INSTALL) -m 0755 ./$(TARGET) $(FAFMBIN)

uninstall:
	-rm -f $(FAFMBIN)/$(TARGET)

dist-src: dep
	mkdir $(TARGET)-$(VERSION)
	cp Makefile $(SOURCES) $(HEADERS) $(TARGET)-$(VERSION)/
	tar czf $(TARGET)-$(VERSION).tgz --owner=0 --group=0 $(TARGET)-$(VERSION)
	rm -r $(TARGET)-$(VERSION)

dist-bin: build
	mkdir $(TARGET)-$(VERSION)-$(ARCH)
	cp $(TARGET) $(TARGET)-$(VERSION)-$(ARCH)
	tar czf $(TARGET)-$(VERSION)-$(ARCH).tgz --owner=0 --group=0 $(TARGET)-$(VERSION)-$(ARCH)
	rm -r $(TARGET)-$(VERSION)-$(ARCH)

test-css:
	-rm ./testfiles/test.min-test.css
	./$(TARGET) -t css ./testfiles/test.css -o ./testfiles/test.min-test.css

test-js:
	-rm ./testfiles/test.min-test.js
	./$(TARGET) -t css ./testfiles/test.js -o ./testfiles/test.min-test.js


