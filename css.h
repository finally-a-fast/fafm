#ifndef CSS_H_INCLUDED
#define CSS_H_INCLUDED

void minify_css(char **minification_buffer);
void minify_css_stripComments(char **minification_buffer);
void minify_css_stripWhitespace(char **minification_buffer);

#endif // CSS_H_INCLUDED
