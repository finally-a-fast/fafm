#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <argp.h>

#include "options.h"
#include "files.h"
#include "minify.h"

int main(int argc, char **argv)
{
  int return_status = EXIT_SUCCESS;
  struct arguments arguments;

  arguments.silent = 0;
  arguments.verbose = 0;
  arguments.type = "css";
  arguments.input_file = "\0";
  arguments.output_file = "\0";
  argp_parse(&argp_parser, argc, argv, 0, 0, &arguments);

  /*
  printf("TYPE = %s\n"
         "INPUT_FILE = %s\n"
         "OUTPUT_FILE = %s\n"
         "VERBOSE = %s\nSILENT = %s\n",
         arguments.type,
         arguments.input_file,
         arguments.output_file,
         arguments.verbose ? "yes" : "no",
         arguments.silent ? "yes" : "no");
  */
  char *minification_buffer = NULL;
  unsigned long original_length = 0;
  unsigned long minified_length = 0;

  if(!load_file(arguments.input_file, &minification_buffer))
  {
    original_length = strlen(minification_buffer);
    return_status = minify(arguments.type, &minification_buffer);

    if(!return_status) {
      minified_length = strlen(minification_buffer);

      if(arguments.output_file[0] != '\0')
      {
        if(!write_file(arguments.output_file, &minification_buffer))
        {
          printf("Done.. minified from %lu to %lu Bytes", original_length, minified_length);
        }
        else
        {
          return_status = EXIT_FAILURE;
        }
      }
      else
      {
        printf("%s", minification_buffer);
      }
    }
  }
  else
  {
    return_status = EXIT_FAILURE;
  }


  free(minification_buffer);

  return return_status;
}
