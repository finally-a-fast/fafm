Finally a fast Minifier
================================================

A CSS and JS Minifier in pure C (11).

Totally not usable yet. Mostly a PoC. Please avoid (for now).

Installation
------------

```
make
```


Use
------------

**Help:**
```
fafm --help
```

**CSS (default):**
```
fafm foo.css -o foo.min.css

fafm -t css foo.css -o foo.min.css

```

**JS:**
```
fafm -t js foo.js -o foo.min.css
```

License
------------

Copy dat Floppy


