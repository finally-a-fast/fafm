
#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pcre2_wrapper.h"

void prcre2_replacestring(char **source_buffer, PCRE2_SPTR re_pattern, PCRE2_SPTR re_replacement)
{
  pcre2_code *re_code;
  pcre2_match_context *re_match_context;
  PCRE2_SIZE re_erroffset, re_outlength;
  PCRE2_UCHAR* re_outbuf;
  int re_result, re_error;

  re_code = pcre2_compile(re_pattern, PCRE2_ZERO_TERMINATED, PCRE2_MULTILINE, &re_error, &re_erroffset, 0);
  if (!re_code)
  {
    PCRE2_UCHAR re_error_buffer[256];
    pcre2_get_error_message(re_error, re_error_buffer, sizeof(re_error_buffer));
    fprintf(stderr, "PCRE2 Error at %d: %s\n\n", (int)re_erroffset, re_error_buffer);

    exit(EXIT_FAILURE);
  }

  re_match_context = pcre2_match_context_create(0);

  re_outlength = 0;
  re_result = pcre2_substitute(
    re_code,
    (PCRE2_SPTR)*source_buffer,
    PCRE2_ZERO_TERMINATED,
    0,
    PCRE2_SUBSTITUTE_GLOBAL | PCRE2_SUBSTITUTE_OVERFLOW_LENGTH | PCRE2_SUBSTITUTE_EXTENDED,
    0,
    re_match_context,
    re_replacement,
    PCRE2_ZERO_TERMINATED,
    0,
    &re_outlength
  );

  if (re_result != PCRE2_ERROR_NOMEMORY)
  {
    PCRE2_UCHAR re_error_buffer[256];
    pcre2_get_error_message(re_result, re_error_buffer, sizeof(re_error_buffer));
    fprintf(stderr, "PCRE2 Error substitute Result: Code %d - %s\n\n", re_result, re_error_buffer);

    exit(EXIT_FAILURE);
  }

  re_outbuf = malloc(re_outlength * sizeof(PCRE2_UCHAR));

  re_result = pcre2_substitute(
    re_code,
    (PCRE2_SPTR)*source_buffer,
    PCRE2_ZERO_TERMINATED,
    0,
    PCRE2_SUBSTITUTE_GLOBAL | PCRE2_SUBSTITUTE_EXTENDED,
    0,
    re_match_context,
    re_replacement,
    PCRE2_ZERO_TERMINATED,
    re_outbuf,
    &re_outlength
  );

  if (re_result < 0)
  {
    PCRE2_UCHAR re_error_buffer[256];
    pcre2_get_error_message(re_result, re_error_buffer, sizeof(re_error_buffer));
    fprintf(stderr, "PCRE2 Error substitute extended Result: Code %d - %s\n\n", re_result, re_error_buffer);
    
    exit(EXIT_FAILURE);
  }

  memset(*source_buffer, 0, strlen(*source_buffer));
  strcpy(*source_buffer, (const char *)re_outbuf);

  free(re_outbuf);
  pcre2_match_context_free(re_match_context);
  pcre2_code_free(re_code);
}
