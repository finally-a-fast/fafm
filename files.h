#ifndef FILES_H_INCLUDED
#define FILES_H_INCLUDED

int load_file(char *file_name, char **input_file_buffer);
int write_file(char *file_name, char **output_file_buffer);

#endif // FILES_H_INCLUDED
