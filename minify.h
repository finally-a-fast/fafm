#ifndef MINIFY_H_INCLUDED
#define MINIFY_H_INCLUDED

int minify(char *input_type, char **minification_buffer);

#endif // MINIFY_H_INCLUDED
