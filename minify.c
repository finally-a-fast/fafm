#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include "minify.h"
#include "css.h"
#include "js.h"

int minify(char *input_type, char **minification_buffer)
{
  //printf("Minifying...%s...\n\n", input_type);
  if(strcmp(input_type, "css") == 0)
  {
    minify_css(minification_buffer);
  }
  else if(strcmp(input_type, "js") == 0)
  {
    minify_js(minification_buffer);
  }
  else
  {
    fprintf(stderr, "Wrong type! Possible types: js or css\n\n");

    return EXIT_FAILURE;
  }

  return EXIT_FAILURE;
}
