#ifndef OPTIONS_H_INCLUDED
#define OPTIONS_H_INCLUDED

#include <argp.h>

extern const char *argp_program_version;

extern const char *argp_program_bug_address;

/*
static char doc[];

static char args_doc[];

static struct argp_option options[];
*/

struct arguments
{
  int silent, verbose;
  char *type;
  char *input_file;
  char *output_file;
};

extern struct argp argp_parser;


#endif // OPTIONS_H_INCLUDED
