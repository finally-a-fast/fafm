#ifndef PCRE2_WRAPPER_H_INCLUDED
#define PCRE2_WRAPPER_H_INCLUDED

#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>

void prcre2_replacestring(char **source_buffer, PCRE2_SPTR re_pattern, PCRE2_SPTR re_replacement);

#endif // PCRE2_WRAPPER_H_INCLUDED
