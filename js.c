/**
  * Currently based on:
  * https://github.com/matthiasmullie/minify
  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "pcre2_wrapper.h"
#include "js.h"

void minify_js(char **minification_buffer)
{
  /*
  fprintf(stderr, "JS not implemented yet!");
  exit(EXIT_FAILURE);
  */

  minify_js_stripComments(minification_buffer);
  minify_js_stripWhitespace(minification_buffer);
 
}

void minify_js_stripComments(char **minification_buffer)
{
  // Remove comments: /* */ (with multiline)
  prcre2_replacestring(minification_buffer, (PCRE2_SPTR)"(?:(?<remove>(?:\\/\\*[\\s\\S]*?\\*\\/)))", (PCRE2_SPTR)"${remove:+}");

   // Remove comments: // (one line comments which are actually not real CSS comments but often used anyway
  prcre2_replacestring(minification_buffer, (PCRE2_SPTR)"(?:(?<remove>(?:\\/\\/.*)))", (PCRE2_SPTR)"${remove:+}");
}


void minify_js_stripWhitespace(char **minification_buffer)
{
    // Remove leading whitespace
    prcre2_replacestring(minification_buffer, (PCRE2_SPTR)"(?:(?<remove>(?:^\\s*)))", (PCRE2_SPTR)"${remove:+}");
    // Remove trailing whitespace
    prcre2_replacestring(minification_buffer, (PCRE2_SPTR)"(?:(?<remove>(?:\\s*$)))", (PCRE2_SPTR)"${remove:+}");

    // replace newlines with a single space
    prcre2_replacestring(minification_buffer, (PCRE2_SPTR)"(?:(?<replace>(?:[\\r\\n]|[\\r]|[\\n]+)))", (PCRE2_SPTR)"${replace:+} ");
}


